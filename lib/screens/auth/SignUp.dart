import 'package:flutter/cupertino.dart';
import 'package:montra_app/helper/Constants.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      navigationBar: CupertinoNavigationBar(
        backgroundColor: CupertinoColors.white,
        border: Border(
          bottom: BorderSide.none,
        ),
        transitionBetweenRoutes: true,
        middle: Text(
          "Sign Up",
          style: styleVeryLarge.copyWith(
              fontWeight: FontWeight.bold, color: dark50),
        ),
        leading: Icon(
          CupertinoIcons.arrow_left,
          color: dark50,
        ),
      ),
      child: CupertinoTheme(
        data: CupertinoThemeData(primaryColor: violet100),
        child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              Container(
                height: 56,
                margin: EdgeInsets.only(bottom: 15),
                child: CupertinoTextField(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(16),
                    ),
                    border: Border.all(color: light60),
                  ),
                  padding: EdgeInsets.only(left: 15),
                  placeholder: "Name",
                  keyboardType: TextInputType.name,
                ),
              ),
              Container(
                height: 56,
                margin: EdgeInsets.only(bottom: 15),
                child: CupertinoTextField(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(16),
                    ),
                    border: Border.all(color: light60),
                  ),
                  padding: EdgeInsets.only(left: 15),
                  placeholder: "Email",
                  keyboardType: TextInputType.emailAddress,
                ),
              ),
              Container(
                height: 56,
                margin: EdgeInsets.only(bottom: 15),
                child: CupertinoTextField(
                  obscureText: true,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(16),
                    ),
                    border: Border.all(color: light60),
                  ),
                  padding: EdgeInsets.only(left: 15),
                  placeholder: "Password",
                  keyboardType: TextInputType.visiblePassword,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
