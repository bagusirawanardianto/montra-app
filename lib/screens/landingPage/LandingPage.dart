import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:montra_app/helper/Constants.dart';
import '../../screens/screens.dart';

class LandingPage extends StatefulWidget {
  const LandingPage({Key key}) : super(key: key);

  @override
  _LandingPageState createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  final introKey = GlobalKey<IntroductionScreenState>();

  Widget _buildImage(String assetName, [double width = 250]) {
    return SvgPicture.asset(
      'assets/icons/$assetName',
      width: width,
      cacheColorFilter: true,
      allowDrawingOutsideViewBox: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    var pageDecoration = PageDecoration(
      bodyFlex: 1,
      imageFlex: 2,
      pageColor: Colors.white,
      bodyAlignment: Alignment.bottomCenter,
      imageAlignment: Alignment.bottomCenter,
      titleTextStyle: styleMostLarge.copyWith(
          fontSize: 32, fontWeight: FontWeight.bold, color: dark50),
      bodyTextStyle: styleLarge.copyWith(color: light20),
      titlePadding: EdgeInsets.symmetric(horizontal: 15.0),
      descriptionPadding: EdgeInsets.all(15),
    );

    return CupertinoPageScaffold(
      backgroundColor: CupertinoColors.white,
      child: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              flex: 4,
              child: IntroductionScreen(
                dotsContainerDecorator: BoxDecoration(
                  color: CupertinoColors.white,
                ),
                controlsPadding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0),
                showDoneButton: false,
                showSkipButton: false,
                showNextButton: false,
                dotsDecorator: DotsDecorator(
                  activeSize: const Size(15.0, 15.0),
                  activeColor: violet100,
                  spacing: const EdgeInsets.symmetric(horizontal: 3.0),
                  activeShape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25.0),
                  ),
                ),
                pages: [
                  PageViewModel(
                    title: "Gain total control of your money",
                    body:
                        "Become your own money manager and make every cent count",
                    image: _buildImage('money.svg'),
                    decoration: pageDecoration,
                  ),
                  PageViewModel(
                    title: "Know where your money goes",
                    body:
                        "Track your transaction easily, with categories and financial report",
                    image: _buildImage('receipt.svg'),
                    decoration: pageDecoration,
                  ),
                  PageViewModel(
                    title: "Planning ahead",
                    body:
                        "Setup your budget for each category so you in control",
                    decoration: pageDecoration,
                    image: _buildImage('checklist.svg'),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 25),
                    width: double.infinity,
                    height: 50,
                    child: CupertinoButton(
                        child: Text(
                          "Sign Up",
                          style:
                              styleLarge.copyWith(fontWeight: FontWeight.bold),
                        ),
                        color: violet100,
                        onPressed: () =>
                            Navigator.pushNamed(context, '/signUp')),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 25),
                    width: double.infinity,
                    height: 50,
                    child: CupertinoButton(
                        child: Text(
                          "Login",
                          style: styleLarge.copyWith(
                              fontWeight: FontWeight.bold, color: violet100),
                        ),
                        color: violet20,
                        onPressed: () => null),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
