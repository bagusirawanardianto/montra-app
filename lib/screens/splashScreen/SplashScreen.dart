import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../../helper/helper.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  void movingToNextScreen() {
    Timer(
        Duration(
          seconds: 2,
        ), () {
      Navigator.pushNamed(context, "/landing");
    });
  }

  @override
  void initState() {
    super.initState();
    movingToNextScreen();
  }

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
      backgroundColor: violet100,
      child: Center(
        child: SvgPicture.asset(
          "assets/icons/montra-logo.svg",
        ),
      ),
    );
  }
}
