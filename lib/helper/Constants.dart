import 'package:flutter/cupertino.dart';

const violet100 = Color(0xFF7F3DFF);
const violet20 = Color(0xFFEEE5FF);

const dark100 = Color(0xFF0D0E0F);
const dark75 = Color(0xFF161719);
const dark50 = Color(0xFF212325);
const dark25 = Color(0xFF292B2D);

const light40 = Color(0xFFE3E5E5);
const light60 = Color(0xFFF1F1FA);
const light20 = Color(0xFF91919F);

const TextStyle styleNormal = TextStyle(fontFamily: 'Inter', fontSize: 12);
const TextStyle styleMedium = TextStyle(fontFamily: 'Inter', fontSize: 14);
const TextStyle styleLarge = TextStyle(fontFamily: 'Inter', fontSize: 16);
const TextStyle styleVeryLarge = TextStyle(fontFamily: 'Inter', fontSize: 18);
const TextStyle styleSuperLarge = TextStyle(fontFamily: 'Inter', fontSize: 20);
const TextStyle styleMostLarge = TextStyle(fontFamily: 'Inter', fontSize: 22);
