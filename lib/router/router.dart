import 'package:flutter/cupertino.dart';
import '../screens/screens.dart';

class MontraApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MontraAppState();
  }
}

class _MontraAppState extends State<MontraApp> {
  @override
  Widget build(BuildContext context) {
    return CupertinoApp(
      initialRoute: '/',
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case '/':
            return CupertinoPageRoute(builder: (_) => SplashScreen());
          case '/landing':
            return CupertinoPageRoute(builder: (_) => LandingPage());
          case '/signUp':
            return CupertinoPageRoute(builder: (_) => SignUp());
          default:
            return CupertinoPageRoute(builder: (_) => SplashScreen());
        }
      },
    );
  }
}
